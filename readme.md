Mit folgenden Git-Befehlen kann ein beliebiger Unterordner aus einem externen Git-Repository in einen beliebigen Unterordner seines eigenen Git-Repositories eingebracht werden. Dabei werden weiterhin die üblichen Commit-Strategien des eigenen Repositories beibehalten. Es empfiehlt sich die Dateien des Unterordners des externen Git-Repositories in einem eigenen Unterordner zu halten, ohne jegliche andere Dateien. Damit kann mit einem einfachen "rm" Befehl der Ordner für den nächsten merge schnell bereinigt werden.

Einmalig zu machen um einzurichten und erstmalig zu verwenden:

Sobald man sich in seinem eigenen Git-Repository befindet, dann bitte unter "viz" zuerst das Remote eingebunden, hier das CDA_Visualization Repository aus dem ELGA GmbH Gitlab.

    git remote add -f -t master viz https://gitlab.com/elga-gmbh/CDA_Visualization.git 

Folgende Zeile ist optional, soll man nur verwenden wenn man mit Ours-Merge-Strategie alle commits einzeln einsehen möchte, ohne werden alle Commits in ein Commit zusammengefasst/gesquashed.

    (git merge -s ours --no-commit viz/master)

Der Subfolder oder SubSubfolder oder sonst wo, wo man den Merge hinhaben möchte, wird ausgesucht.

    git read-tree --prefix=mySubFolder/mySubSubFolder/ -u viz/master:ELGA_Referenzstylesheet

Änderungen werden ins eigene Repository commited

    git commit

Ein weiteres Ausführen wenn Änderungen anstehen:

Folgende erste Zeile ist wieder optional, soll man nur verwenden wenn man mit Ours-Merge-Strategie alle commits einzeln einsehen möchte, ohne werden alle Commits in ein Commit zusammengefasst/gesquashed.

    (git merge -s ours --no-commit viz/master)

    git rm -rf mySubFolder/mySubSubFolder

    git read-tree --prefix=mySubFolder/mySubSubFolder/ -u viz/master:ELGA_Referenzstylesheet

    git commit
